import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './fonts/Hatsch.ttf'
import './fonts/lilstuart.ttf'
import './fonts/Cormorant.ttf'
import './fonts/Sarabun.ttf'
import './fonts/Sarabun-Regular.ttf'
import './fonts/Sarabun-ExtraBold.ttf'
import './fonts/Sarabun-SemiBold.ttf'
import './fonts/lil stuart.ttf'
ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,

  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
