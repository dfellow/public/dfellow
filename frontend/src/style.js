import React  from 'react';
import styled from 'styled-components'


const Bg = styled.div`
background: #2B313B;
`;

const BgW = styled.div`
background: #EBE8D8;
`;


const Title = styled.div`
color: #EBE8D8;
padding-top:10%; 
font-family: Hatsch;
text-align: right;
font-size: 2vw;


${props => props.to && `
  color: #FF614C;
  font-family: lil;
  font-size: 3.5vw;
  margin-top :0%;
  margin-left: -10%;
  text-align: center;
`}
${props => props.right && `
  color: #EBE8D8;
  font-family: Hatsch;
  text-align: left;
  width : 100%;
`}
@media only screen and (max-width: 600px) {
  font-size: 4vw;
  padding-top:20%; 
  ${props => props.to && `
    font-size: 5.5vw;
  `}
}
`;

const Dfellow = styled.div`
color: #FFDA1A;
font-family: lil;
font-size: 35vw;
text-shadow: 2px 2px black;
margin-top : -15%;
margin-left :-10%;
text-align: left;
${props => props.team && `
color: #EBE8D8;
font-family: Hatsch;
text-align: center;
font-size: 7vw;
margin-top : -35%;
margin-left :38%;


`}
@media only screen and (max-width: 600px) {
    font-size: 50vw;
    padding-top:0%; 
    margin-top: -110%;
    margin-left :-30%;
    ${props => props.team && `
      position: absolute;
      font-size: 9vw;
      padding-top:0%; 
      margin-top : -103%;
      margin-left :65%;
    `}
}

  /* text-shadow: 2px 2px #ff0000; */
`;

const TriangleD = styled.div`  
margin-top : -8%;
@media only screen and (max-width: 600px) {
  margin-top : -14%;

}
`;
const Triangle = styled.div`
width: 0;
height: 0;
border-bottom: 100vw solid #36C9DC;
border-bottom: calc(var(--vh, 1vh) * 100) solid #36C9DC;
border-right: 55vw solid transparent; 
border-right: calc(var(--vh, 1vh) * 55) solid transparent;    
@media only screen and (max-width: 600px) {
  border-bottom: 20vw solid #36C9DC;
  border-bottom: calc(var(--vh, 1vh) * 100) solid #36C9DC;
  border-right: 25vw solid transparent; 
  border-right: calc(var(--vh, 1vh) * 25) solid transparent;    
}

`;
const TriangleR = styled.div`
width: 0;
height: 0;
border-bottom: 100vh solid #FF614C;
border-left: 55vh solid transparent;      
float : right;
@media only screen and (max-width: 600px) {
  border-bottom: 100vh solid #FF614C;
  border-left: 55vh solid transparent;    
  border-left: calc(var(--vh, 1vh) * 25) solid transparent;    

}
`;
const Describe  = styled.div`
color: #EBE8D8;
font-family: Hatsch;
text-align: right;
font-size: 1.8vw;   
margin-left : 1%;
${props => props.green && `
  color: #BFDC36;
  text-align: center;

`} 
${props => props.left && `
  text-align: left;
`} 
 @media only screen and (max-width: 600px) {
  font-size: 3vw;   
}
`;
const DesDiv = styled.div`  
margin-top : -20.6%;
padding-bottom : 2%;
@media only screen and (max-width: 600px) {
margin-top : 25%;

}
`;
const AboutD  = styled.div`
color: #2B313B;
font-family: Hatsch;
text-align: center;
padding-top : 3%;
font-size: 2vw;   
padding-bottom : 3%;
${props => props.rotateR && `
  transform: rotate(-90deg);
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
  margin-left : 80%;

`} 
${props => props.rotateL && `
  transform: rotate(-270deg);
  -webkit-transform: rotate(-270deg);
  -moz-transform: rotate(-270deg);
  -ms-transform: rotate(-270deg);
  -o-transform: rotate(-270deg);
  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
  margin-right : 80%;

`} 

`;
const AboutLogo  = styled.div`
color: #BFDC36;
font-family: Hatsch;
text-align: center;
padding-top : 8%;
font-size: 6.5vw;   
${props => props.under && `
  padding-top : 0%;
  max-width: 23%;
  height: 3vh;
  margin: auto;
  background: #FF614C;
  margin-left : 38%;
  margin-top : -2%;
`} 
${props => props.us && `
  padding-top : 0%;
  color: #2B313B;
  font-family: lil;
  text-align: center;
  font-size: 18vw;  
  margin-top : -18%;
`} 
${props => props.des && `
padding-top : 10%;
color: #2B313B;
font-family: cormo;
text-align: center;
font-size: 1.5vw;  
margin-top : -18%;
padding-bottom : 8%;
`} 


`;

const OurWork  = styled.div`

${props => props.our && `
color: #EBE8D8;
font-family: Hatsch;
text-align: center;
font-size: 4vw;  
margin-top : 15%;
margin-left : 12%;

`} 
${props => props.work && `
color: #FF614C;
font-family: lil;
text-align: center;
font-size: 12vw; 
margin-top : -10%;

`} 
${props => props.process && `
margin-top : 10%;

`} 
${props => props.processNum && `
margin-top : 0%;
color: #EBE8D8;
font-family: Hatsch;
font-size: 4vw; 
position: relative;

`} 
${props => props.name && `

color: #EBE8D8;
padding-top :3%;
padding-bottom :0%;
font-family: Hatsch;
font-size: 2vw; 

`} 
${props => props.date && `

color: #EBE8D8;
padding-bottom :0%;
font-family: cormo;
font-size: 2vw; 

`} 
@media only screen and (max-width: 600px) {
${props => props.our && `
font-size: 7vw;  
margin-left : 17%;
`} 
${props => props.work && `
  font-size: 15vw; 
  text-align : center;
  margin-top : -13%;

`}

}
`;
