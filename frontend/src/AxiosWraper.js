import axios from 'axios';

const axiosWarpper = axios.create({

    baseURL: 'https://api.dfellow.com/api/v1'
});
axiosWarpper.defaults.headers['Content-Type'] = "application/json";

export default axiosWarpper;