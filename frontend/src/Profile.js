import React from 'react';
import  logo from './logo.svg'
import styled from 'styled-components';
import { Card, Image } from 'antd';

const imgwidth = '40vw';
const pos = 'back\nend';
const aboutText = 'About gagagagaaggagagagaagagag\nAbout ffafafaafafafafafaafafafafafa';
const personalText = 'test\ntest\ntest\n';

const Backgound = styled.div`
    height: auto;
    background-color: #2B313B;
`;

const H1Position = styled.h1`
    position: absolute;
    z-index: 3;
    left: 3vw;
    top: 20vh;
    width: 30vw;
    word-wrap: break-word;
    padding: 0px;
    margin: 0px 0px;
    text-align: left;
    font-size: 10rem;
    color: #FFDA1A;
    text-shadow: 0px 3px 6px #000000;
    text-transform: uppercase;
`;

const TopDiv = styled.div`
    height: 50vh;
    display: inline;
`;

const ImgProfile = styled.div`
    position: relative;
    height: fit-content;
    width: fit-content;
    justify-content: center;
    padding: 5vw;
    margin: 0px auto;

`;

const ProfileTitle = styled.div`
    position: relative;
    z-index: 2;
    height: fit-content;
    width: fit-content;
    justify-content: center;
    padding: 5vw;
    background-color: #EBE8D8;
`;

const NameTitle  = styled.h1`
    color: #2B313B;
    // font: normal normal normal 50px/150px Lil Stuart;
    text-align: center;
    margin: 0 0;
`;

const BGCircle1 = styled.svg`
    position: absolute;
    z-index: 1;
    left: 60%;
    top: 0px;
    height: 600px;
    width: 600px;
`;

const BGCircle2 = styled.svg`
    position: absolute;
    z-index: 1;
    left: -150px;
    top: 50%;
    height: 500px;
    width: 500px;
`;

const TitleAbout = styled.div`
    text-align: center;
    font-size: 3rem;
    background-color: #FFFFFF;
    margin-top: 15vh;
`;

function NewlineText(props) {
    const text = props.text;
    return text.split('\n').map(str => <p>{str}</p>);
}

const PersonalProfile = styled.div`
    width: 70vw;
    // margin-left: -2%;
    padding: 8vh 3vw 2vh 3vw;
    background-color: #BFDC36;
`;

const  PersonalTitle = styled.h1`
    position: relative;
    left: -2%;
    top: 7vh;
    font-size: 13rem;
    color: #EBE8D8;
    text-shadow: 0px 3px 6px #2B313B;
    margin: 0 0;
`;

const SkillProfile = styled(PersonalProfile)`
    position: relative;
    margin-left: 23vw;
    // float: right;
    background-color: #FFDA1A;
`;

const SkillTitle = styled(PersonalTitle)`
    z-index: 2;
    margin-right: 50vw;
    text-align: right;
    color: #36C9DC;
    text-shadow: 0px 3px 6px #2B313B;
    // transform: matrix(0.95, -0.33, 0.33, 0.95, 0, 0);
`;

const ProjectTitle = styled(PersonalTitle)`
    color: #FFDA1A;
`;

const ProjectList = styled(PersonalProfile)`
    background-color: #36C9DC;
`;

const BGCircle3 = styled.svg`
    position: relative;
    z-index: 1;
    // margin-left: -2vw;
    height: 1000px;
    width: 100%;
`;

export default class Profile extends React.Component {


    render(){
        return (
            <Backgound>
                <TopDiv>
                    <ImgProfile>
                        <ProfileTitle>
                            <Image src={logo} width={imgwidth} /> 
                            <NameTitle>Name</NameTitle>    
                        </ProfileTitle> 
                        <BGCircle1>
                            <circle cx="300" cy="300" r="300" fill="#36C9DC" />  
                        </BGCircle1> 
                        <BGCircle2>
                            <circle cx="300" cy="300" r="150" fill="#BFDC36" />    
                        </BGCircle2>
                    </ImgProfile>            
                    <H1Position>{pos}</H1Position>                         
                </TopDiv>

                <TitleAbout>
                    <NewlineText text={aboutText} />
                </TitleAbout>

                <PersonalTitle>
                    Personal profile
                </PersonalTitle>
                <PersonalProfile>
                    <NewlineText text={personalText} />
                </PersonalProfile>

                <SkillTitle>
                    Skill
                </SkillTitle>
                <SkillProfile>
                    <NewlineText text={personalText} />
                </SkillProfile>

                <ProjectTitle>
                    Project
                </ProjectTitle>
                <ProjectList>
                    <NewlineText text={personalText} />
                </ProjectList>

                <BGCircle3>
                    <circle cx="400" cy="900" r="800" fill="#EBE8D8" />    
                    <circle cx="1500" cy="1000" r="500" fill="#FF614C" />    
                </BGCircle3>
            </Backgound>
        )}
}