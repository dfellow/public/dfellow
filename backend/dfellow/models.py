from django.db import models
from django.core.validators import RegexValidator
# Create your models here.

class Image(models.Model):
    image = models.ImageField()

    def __str__(self):
        return self.image.name

class Member(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    nickname = models.CharField(max_length=20)
    position = models.CharField(max_length=100)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)
    order = models.PositiveSmallIntegerField()
    is_use = models.BooleanField()

    def __str__(self):
        return "{}-{}".format(self.is_use,self.name)
    
class Project(models.Model):
    name = models.CharField(max_length=100)
    short_description = models.CharField(max_length=300)
    description = models.TextField()
    image = models.ManyToManyField(Image)
    member = models.ManyToManyField(Member)
    link = models.URLField()
    is_use = models.BooleanField()

    def __str__(self):
        return "{}-{}".format(self.is_use,self.name)

class Contact(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    title = models.CharField(max_length=300)
    description = models.TextField()

class Tool(models.Model):
    name = models.CharField(max_length=100)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)
    is_use = models.BooleanField()

    def __str__(self):
        return "{}-{}".format(self.is_use,self.name)


tel_validation = RegexValidator(r'^0[0-9]+$', 'Telephone Allow Only')
class Profile(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    about_me = models.TextField()
    tel = models.CharField(max_length=13, validators=[tel_validation])
    mail = models.EmailField()
    address = models.TextField()
    education = models.TextField()
    programing_language = models.TextField()
    tool = models.ManyToManyField(Tool)
    ability = models.TextField()
    cerification = models.TextField()
    project = models.ManyToManyField(Project)
    is_use = models.BooleanField()

    def __str__(self):
        return "{}-{}".format(self.is_use,self.member.name)
