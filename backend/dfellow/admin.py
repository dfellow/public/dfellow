from django.contrib import admin
from .models import Project, Image, Member, Profile, Contact, Tool
# Register your models here.
class ProjectAdmim(admin.ModelAdmin):
    list_display = [f.name for f in Project._meta.fields]
    list_editable = ['is_use']
    list_filter = ['is_use']
admin.site.register(Project, ProjectAdmim)

class ImageAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Image._meta.fields]
admin.site.register(Image, ImageAdmin)

class MemberAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Member._meta.fields]
    list_editable = ['is_use','order']
admin.site.register(Member, MemberAdmin)

class ProfileAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Profile._meta.fields]
    list_editable = ['is_use']
admin.site.register(Profile, ProfileAdmin)

class ContactAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Contact._meta.fields]
admin.site.register(Contact, ContactAdmin)

class ToolAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Tool._meta.fields]
    list_editable = ['is_use']
admin.site.register(Tool, ToolAdmin)