from rest_framework import viewsets, mixins
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .serializers import ProjectSerializers, MemberSerializers, ImageSerializers, ProfileSerializers, \
                        ContactSerializers, ToolSerializers, ProfileAllSerializers
from dfellow.models import Project, Member, Image, Profile, Contact, Tool

class ImageViewset(viewsets.GenericViewSet,
                    mixins.ListModelMixin):

    serializer_class = ImageSerializers
    model = Image

    def get_queryset(self):
        return self.model.objects.all()
    
class ProjectViewset(viewsets.GenericViewSet,
                    mixins.ListModelMixin):

    serializer_class = ProjectSerializers
    model = Project

    def get_queryset(self):
        return self.model.objects.filter(is_use=True)

class MemberViewset(viewsets.GenericViewSet,
                    mixins.ListModelMixin):
    
    serializer_class = MemberSerializers
    model = Member

    def get_queryset(self):
        return self.model.objects.filter(is_use=True).order_by('order')


# class ProfileViewset(viewsets.ViewSet):
#     model = Profile
#     def list(self, request):
#         queryset = self.model.objects.filter(is_use=True, member__is_use=True).order_by('member__order')
#         serializer = ProfileAllSerializers(queryset, many=True, read_only=True)
#         return Response(serializer.data)

#     def retrieve(self, request, pk=None):
#         queryset = self.model.objects.filter(is_use=True, member__is_use=True).order_by('member__order')
#         data = get_object_or_404(queryset, pk=pk)
#         serializer = ProfileSerializers(data)
#         return Response(serializer.data)
class ProfilesViewset(viewsets.GenericViewSet,
                    mixins.ListModelMixin):
    serializer_class = ProfileAllSerializers
    model = Profile
    def get_queryset(self):
        return self.model.objects.filter(is_use=True, member__is_use=True).order_by('member__order')

class ProfileViewset(viewsets.GenericViewSet,
                    mixins.RetrieveModelMixin):
    serializer_class = ProfileSerializers
    model = Profile
    def get_queryset(self):
        return self.model.objects.filter(is_use=True, member__is_use=True).order_by('member__order')


class ContactViewset(viewsets.ModelViewSet):
    serializer_class = ContactSerializers
    model = Contact

    def get_queryset(self):
        return []

class ToolViewset(viewsets.GenericViewSet,
                mixins.ListModelMixin):
    
    serializer_class = ToolSerializers
    model = Tool

    def get_queryset(self):
        return self.model.objects.filter(is_use=True)
    