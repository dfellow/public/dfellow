from rest_framework import routers
from .viewsets import ProjectViewset, MemberViewset, ImageViewset, ProfilesViewset, ContactViewset, ToolViewset, ProfileViewset

router = routers.DefaultRouter()
router.register(r'project', ProjectViewset, basename='project')
router.register(r'member', MemberViewset, basename='member')
router.register(r'profiles', ProfileViewset, basename='profile')
router.register(r'profiles', ProfilesViewset, basename='profiles')
router.register(r'contact', ContactViewset, basename='contact')
router.register(r'tool', ToolViewset, basename='tool')

router.register(r'image', ImageViewset, basename='image')