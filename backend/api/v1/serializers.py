from rest_framework import serializers
from dfellow.models import Project, Image, Member, Profile, Contact, Tool
exclude = ['is_use','order']
class ImageSerializers(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image']

class MemberSerializers(serializers.ModelSerializer):
    image = ImageSerializers()
    class Meta:
        model = Member
        fields = [f.name for f in Member._meta.fields if f.name not in exclude] + ['image']

class ProjectSerializers(serializers.ModelSerializer):
    image = ImageSerializers(many=True)
    member = MemberSerializers(many=True)
    class Meta:
        model = Project
        fields = [f.name for f in Project._meta.fields if f.name not in exclude] + ['image', 'member']


class ContactSerializers(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = [f.name for f in Contact._meta.fields]

class ToolSerializers(serializers.ModelSerializer):
    image = ImageSerializers()
    class Meta:
        model = Tool
        fields = [f.name for f in Tool._meta.fields if f.name not in exclude] + ['image']

class ProfileAllSerializers(serializers.ModelSerializer):
    member = MemberSerializers()
    class Meta:
        model = Profile
        fields = fields = [f.name for f in Profile._meta.fields if f.name not in exclude] + ['member']

class ProfileSerializers(serializers.ModelSerializer):
    member = MemberSerializers()
    tool = ToolSerializers(many=True)
    project = ProjectSerializers(many=True)
    class Meta:
        model = Profile
        fields = fields = [f.name for f in Profile._meta.fields if f.name not in exclude] + ['member', 'tool', 'project']
